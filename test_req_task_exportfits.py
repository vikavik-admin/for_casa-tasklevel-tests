##########################################################################
# test_req_task_exportfits.py
#
# Copyright (C) 2018
# Associated Universities, Inc. Washington DC, USA.
#
# This script is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for morgit e details.
#
# [Add the link to the JIRA ticket here once it exists]
#
# Based on the requirements listed in plone found here:
# https://casa.nrao.edu/casadocs-devel/stable/global-task-list/task_exportfits/about
#
#
##########################################################################

import sys
import os
import unittest
import shutil
import argparse


def skipIfMissingModule(required_module):
    import os
    try:
        __import__(required_module)
        flag = True
    except ImportError:
        flag = False
    def deco(f):
        if not CASA6:
            return deco
        
        def wrapper(self, *args, **kwargs):
            if not flag:
                # If there is a strict flag run the tests as normal
                print(sys.argv)
                if strict:
                    f(self)
                    pass
                else:
                    # Module ImportError and no strict flag
                    self.skipTest("ModuleNotFoundError: No module named '{}'".format(required_module))
            else:
                f(self)
        return wrapper
    return deco


CASA6 = False

try:
    import casatools
    from casatasks import exportfits, imhead
    CASA6 = True
    try:
        from astropy.io import fits
    except:
        pass
except ImportError:
    from __main__ import default
    from tasks import exportfits, imhead
    from taskinit import *
    import pyfits as fits


### Data ###

if CASA6:
    casaimage = casatools.ctsys.resolve('image/ngc5921.clean.image')
else:
    if os.path.exists(os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req'):
        casaimage = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/image/ngc5921.clean.image'
    else:
        casaimage = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/image/ngc5921.clean.image'
    
fitsfile = 'test.fits'
fitsfile2 = 'test2.fits'

class exportfits_test(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        pass
    
    def setUp(self):
        pass
    
    def tearDown(self):
        if os.path.exists(fitsfile):
            os.remove(fitsfile)
        if os.path.exists(fitsfile2):
            os.remove(fitsfile2)
    
    @classmethod
    def tearDownClass(cls):
        pass
    
    def test_makesFits(self):
        '''
            test_makesFits
            -----------------
            
            Test that export fits will produce a fits file
        '''
        
        exportfits(imagename=casaimage, fitsimage=fitsfile)
        
        self.assertTrue(os.path.exists(fitsfile))
        
    def test_hasDim(self):
        '''
            test_hasDim
            ---------------
            
            Test that the default setting of exportfits creates a fits file with 4 dimensions (Ra, Dec, radio freq, polarization)
            (these may actually be ['Right Ascension', 'Declination', 'Frequency', 'Stokes'] in the fits file)
        '''
        
        dimList = ['Right Ascension', 'Declination', 'Frequency', 'Stokes']
        
        exportfits(imagename=casaimage, fitsimage=fitsfile)
        
        self.assertTrue(all(dimList == imhead(fitsfile)['axisnames']))
        
    def test_dropstokes(self):
        '''
            test_dropstokes
            -------------------
            
            Test that dropstokes = True will cause the polarization axis to be omitted
        '''
        
        exportfits(imagename=casaimage, fitsimage=fitsfile, dropstokes=True)
        
        self.assertTrue('Stokes' not in imhead(fitsfile)['axisnames'])
        
    @skipIfMissingModule('astropy.io.fits')
    def test_velocity(self):
        '''
            test_velocity
            ----------------
            
            Test that velocity = True will cause the spectral axis to have units of velocity rather than frequency
            
        '''

        exportfits(imagename=casaimage, fitsimage=fitsfile, velocity=True)

        hdul = fits.open(fitsfile)
        specUnit = hdul[0].header['CUNIT3']
        
        # Check That the frequncy is no longer used. It would be better to check for the existance of the velocity units (should it be in km/s or m/s?
        self.assertTrue(specUnit == r'm/s')
        
    def test_stokeslast(self):
        '''
            test_stokeslast
            --------------------
            
            Test that stokeslast = Flase switches the positions of the Stokes and Frequency axis
        '''

        exportfits(imagename=casaimage, fitsimage=fitsfile, stokeslast=False)
        
        self.assertTrue('Stokes' == imhead(fitsfile)['axisnames'][2])
        
    def test_dropdeg(self):
        '''
            test_dropdeg
            --------------
            
            Test that dropdeg = True will cause degenerate axis to be omitted. (axis of length 1) In this case stokes?
            Find and check total-intensity continuum image ends with only RA and Dec
            
        '''
        exportfits(imagename=casaimage, fitsimage=fitsfile, dropdeg=True)
        
        self.assertTrue('Stokes' not in imhead(fitsfile)['axisnames'])

    @skipIfMissingModule('astropy.io.fits')
    def test_bitpix(self):
        '''
            test_bitpix
            --------------
            
            test that the bitpix parameter will change the data type between float32 and int16
        '''

        exportfits(imagename=casaimage, fitsimage=fitsfile, bitpix=16)
        exportfits(imagename=casaimage, fitsimage=fitsfile2, bitpix=-32)
        
        data = fits.open(fitsfile)
        datatype = data[0].header['bitpix']
        
        data2 = fits.open(fitsfile2)
        datatype2 = data2[0].header['bitpix']
        
        self.assertTrue(16 == datatype)
        self.assertTrue(-32 == datatype2)
        
    def test_bitpixsize(self):
        '''
            test_bitpix
            ---------------
            
            Test that bitpix=16 results in a smaller fits file than bitpix=-32
        '''
        
        exportfits(imagename=casaimage, fitsimage=fitsfile, bitpix=16)
        exportfits(imagename=casaimage, fitsimage=fitsfile2, bitpix=-32)
        
        bit16Size = os.path.getsize(fitsfile)
        float32Size = os.path.getsize(fitsfile2)
        
        self.assertTrue(float32Size > bit16Size)

    @skipIfMissingModule('astropy.io')
    def test_maxpix(self):
        '''
            test_maxpix
            --------------
            
            Test that maxpix changes the 16 bit max data range
        '''

        exportfits(imagename=casaimage, fitsimage=fitsfile, bitpix=16, maxpix=1.00)
        
        data = fits.open(fitsfile)
        datamax = data[0].header['datamax']
        
        self.assertTrue(datamax == 1.00)

    @skipIfMissingModule('astropy.io.fits')
    def test_minpix(self):
        '''
            test_minpix
            -------------
            
            Test that minpix changes the 16 bit min data range
        '''

        exportfits(imagename=casaimage, fitsimage=fitsfile, bitpix=16, minpix=-1.00)
        
        data = fits.open(fitsfile)
        datamin = data[0].header['datamin']
        
        self.assertTrue(datamin == -1.00)

    @skipIfMissingModule('astropy.io.fits')
    def test_optical(self):
        '''
            test_optical
            ---------------
            
            Test that optical = True uses the optical rather than the radio velocity convention
            NOTE: I'm not sure if I'm looking at the right thing. It's all I found about velocity that changed (come back to this)
        '''
        exportfits(imagename=casaimage, fitsimage=fitsfile, optical=True)
        
        data = fits.open(fitsfile)
        optical = data[0].header['velref']
        
        self.assertTrue(1 == optical)
        
    def test_overwrite(self):
        '''
            test_overwrite
            -----------------
            
            Test that the overwrite parameter allows an existing fitsfile to be overwritten
        '''
        
        exportfits(imagename=casaimage, fitsimage=fitsfile)
        exportfits(imagename=casaimage, fitsimage=fitsfile, overwrite=True)

    @skipIfMissingModule('astropy.io.fits')
    def test_history(self):
        '''
            test_history
            ---------------
            
            Test that history toggles history info being displayed is toggled on or off
        '''

        exportfits(imagename=casaimage, fitsimage=fitsfile, history=True)
        
        data = fits.open(fitsfile)
        historylist = data[0].header['history']
        
        exportfits( imagename=casaimage, fitsimage=fitsfile2, history=False)
        
        data = fits.open(fitsfile2)
        with self.assertRaises(KeyError):
            historylist = data[0].header['history']
    
    
def suite():
    return[exportfits_test]

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--strict', action="store_true", default=False, dest='strict_flag')
    parser.add_argument('unittest_args', nargs='*')

    args = parser.parse_args()
    strict = False
    if args.strict_flag:
        strict = True
    sys.argv[1:] = args.unittest_args
    unittest.main() 
