##########################################################################
# test_req_task_importgmrt.py
#
# Copyright (C) 2018
# Associated Universities, Inc. Washington DC, USA.
#
# This script is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# [Add the link to the JIRA ticket here once it exists]
#
# Based on the requirements listed in plone found here:
# https://casa.nrao.edu/casadocs/casa-5.4.0/global-task-list/task_importgmrt/about
#
# test import: Checks that importing works and doesn't accept fake inputs
# test flagfile: Checks the flagfile param and that fake flag files cause failure
#
##########################################################################

CASA6 = False
try:
    import casatools
    from casatasks import importgmrt, rmtables
    CASA6 = True
except ImportError:
    from __main__ import default
    from tasks import *
    from taskinit import *
import sys
import os
import unittest
import shutil

# DATA #
if CASA6:
    datapath = casatools.ctsys.resolve('fits/PSR_610.LTA_RRLL.RRLLFITS.fits')
    #flagpath = casatools.ctsys.resolve('caltables/psr_610.ltb##07DT025.FLAGS')
    #flagpath = casatools.ctsys.resolve('caltables/22_015_11may2012.lta##22_015.FLAGS')
    flagpath = '/home/casa/data/trunk/regression/unittest/flagdata/BigOnlineFlags.txt'
else:
    if os.path.exists(os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/fits/PSR_610.LTA_RRLL.RRLLFITS.fits'):
        datapath = (os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/fits/PSR_610.LTA_RRLL.RRLLFITS.fits')
        #flagpath = (os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/caltables/psr_610.ltb##07DT025.FLAGS')
        #22_015_11may2012.lta##22_015.FLAGS
        #flagpath = (os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/caltables/22_015_11may2012.lta##22_015.FLAGS')
        flagpath = '/home/casa/data/trunk/regression/unittest/flagdata/BigOnlineFlags.txt'
        
    else:
        datapath = os.environ.get('CASAPATH').split()[0] + '/bin/PSR_610.LTA_RRLL.RRLLFITS.fits'
        flagpath = os.environ.get('CASAPATH').split()[0] + '/bin/psr_610.ltb##07DT025.FLAGS'
    
class importgmrt_test(unittest.TestCase):
    
    def setUp(self):
        if not CASA6:
            default(importgmrt)
    
    def tearDown(self):
        rmtables('converted.ms')
    
    @classmethod
    def tearDownClass(cls):
        shutil.rmtree('converted.ms.flagversions')
    
    def test_import(self):
        '''test import: test an import with both a real and fake ms'''
        # Make sure the fits file imports then remove the table

        self.assertTrue(importgmrt(fitsfile=datapath, vis='converted.ms'), msg='import fails when using a valid MS')
        rmtables('converted.ms')
        # Done without providing the vis param
        self.assertTrue(importgmrt(fitsfile=datapath))
        # Check that it won't run and create the same file again
        self.assertFalse(importgmrt(fitsfile=datapath))
        # Remove the created file, without vis it is made in the file location
        if CASA6:
            rmtables(casatools.ctsys.resolve('fits/PSR_610.LTA_RRLL.RRLLFITS.MS'))
        else:
            rmtables(os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/fits/PSR_610.LTA_RRLL.RRLLFITS.MS')
        # test again with a fake ms. This shouldn't make a MS
        if CASA6:
            with self.assertRaises(AssertionError, msg='An error is not raised when using a fake MS'):
                importgmrt(fitsfile='notareal.ms', vis='notmade.ms')
        else:
            self.assertFalse(importgmrt(fitsfile='notareal.ms', vis='notmade.ms'), msg='An error is not raised when using a fake MS')
      
    def test_flagfile(self):
        '''test flagfile: test that import gmrt works when given a proper flag file'''
        # Make sure this runs with real flags and fails with fake ones
        self.assertTrue(importgmrt(fitsfile=datapath, flagfile=flagpath, vis='converted.ms'), msg='Fails when provided with a valid flag file')
        # Need to remove the ms inbetween or else the nex line will fail for the wrong reasons
        rmtables('converted.ms')
        self.assertFalse(importgmrt(fitsfile=datapath, flagfile='fakeflag.FLAGS', vis='converted.ms'), msg='An error is not raised when given a fake flag file')
    
    
def suite():
    return[importgmrt_test]

if __name__ == '__main__':
    unittest.main()
    
