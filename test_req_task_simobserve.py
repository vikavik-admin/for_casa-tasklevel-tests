##########################################################################
# test_req_task_simobserve.py
#
# Copyright (C) 2018
# Associated Universities, Inc. Washington DC, USA.
#
# This script is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# [Add the link to the JIRA ticket here once it exists]
#
# Based on the requirements listed in plone found here:
# https://casa.nrao.edu/casadocs-devel/stable/global-task-list/task_simobserve/about
#
#
##########################################################################

CASA6 = False
try:
    import casatools
    from casatasks import simobserve, rmtables, exportfits, casalog
    tb = casatools.table()
    cl = casatools.componentlist()
    ia = casatools.image()
    qa = casatools.quanta()
    ms = casatools.ms()
    CASA6 = True
except ImportError:
    from __main__ import default
    from tasks import *
    from taskinit import *
import sys
import os
import unittest
import shutil
import numpy as np
import filecmp

### DATA ###

if CASA6:
    fitsfile = casatools.ctsys.resolve('fits/1904-66_AIR.fits')
else:
    if os.path.exists(os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req'):
        fitsfile = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/fits/1904-66_AIR.fits/'
    else:
        fitsfile = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/fits/1904-66_AIR.fits/'

default_incell = '0.1arcsec'
default_incenter = '85GHz'
default_inwidth = '10MHz'

project_name = 'testproj'
genfits = 'Gaussian.fits'

### Make simulated image(s) single plane ###
direction = "J2000 10h00m00.0s -30d00m00.0s"
cl.done()
cl.addcomponent(dir=direction, flux=1.0, fluxunit='Jy', freq='230.0GHz', shape="Gaussian", majoraxis="0.1arcmin", minoraxis='0.05arcmin', positionangle='45.0deg')

ia.fromshape("Gaussian.im",[256,256,1,1],overwrite=True)
cs=ia.coordsys()
cs.setunits(['rad','rad','','Hz'])
cell_rad=qa.convert(qa.quantity("0.1arcsec"),"rad")['value']
cs.setincrement([-cell_rad,cell_rad],'direction')
cs.setreferencevalue([qa.convert("10h",'rad')['value'],qa.convert("-30deg",'rad')['value']],type="direction")
cs.setreferencevalue("230GHz",'spectral')
cs.setincrement('1GHz','spectral')
ia.setcoordsys(cs.torecord())
ia.setbrightnessunit("Jy/pixel")
ia.modify(cl.torecord(),subtract=False)
exportfits(imagename='Gaussian.im',fitsimage='Gaussian.fits',overwrite=True)

logpath = casalog.logpath()

class simobserve_test(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        pass
    
    def setUp(self):
        if not CASA6:
            default(simobserve)
    
    def tearDown(self):
        shutil.rmtree(project_name)
        shutil.rmtree('gen')
        os.remove('testlog.log')
    
    @classmethod
    def tearDownClass(cls):
        pass
    
    def test_projectCreated(self):
        '''
            test_projectCreated
            -----------------------
            
            Test that simobserve creates a project directory when run properly
            
            Make sure this works with a fits file and a component list
        '''
        
        simobserve(project=project_name, skymodel=fitsfile ,incell=default_incell, incenter=default_incenter, inwidth=default_inwidth)
        self.assertTrue(os.path.exists('testproject'))
        
        simobserve(project='gen', skymodel=genfits)
        self.assertTrue(os.path.exists('gen'))
        
    def test_mesSetCreated(self):
        '''
            test_mesSetCreated
            ----------------------
            
            Test that simobserve creates a measurment set
            This should be a total power MS
            Make sure this works with both fits and component list
            NOTE: Come back to this one
        '''
        
        simobserve(project=project_name, skymodel=fitsfile, incell=default_incell, incenter=default_incenter, inwidth=default_inwidth)
        self.assertTrue(os.path.exists('testproj/testproj.alma.out10.ms'))
        
        simobserve(project='gen', skymodel=genfits)
        self.assertTrue(os.path.exists('gen'))
        
        # How do I find if it's a total power MS?
        
    def test_outputs(self):
        '''
            test_outputs
            --------------
            
            Test that the outputs that were specified in the documentation are sucessfully created.
            
            These include files ending in: skymodel, skymodel.flat.regrid.conv, skymodel.png, ptg.txt, quick.psf, ms, noisy.ms, observe.png, and  simobserve.last
            
            Some of these depend on input parameters. 
            NOTE: What is supposed to be here by default?
        '''
        
        simobserve(project='gen', skymodel=genfits)
        # should .flat be flat.regrid.conv? not sure where there regrid option comes in
        # some parameters change these endingslist
        filebeginning = 'gen.alma.out10.'
        endingslist = ['ms', 'noisy.ms', 'observe.png', 'ptg.txt', 'quick.psf', 'simobserve.last', 'skymodel', 'skymodel.flat', 'skymodel.png']
        
        for ending in endingslist:
            self.assertTrue(os.path.exists('gen/' + filebeginning + ending))
            
    def test_inbright(self):
        '''
            test_inbright
            ----------------
            
            This parameter scales the surface brightness of the brightest pixel
            
            NOTE not sure where to find this in the measurment set or generated image (in skymodel)
        '''
        
        simobserve(project='gen', skymodel=genfits, inbright='1.5Jy/pixel')
        
        tb.open('gen/gen.alma.out10.skymodel')
        self.assertTrue(np.amax(tb.getcol('map')) == 1.5)
        tb.close()
        
        # what should the default value be?
        simobserve(project='gen', skymodel=genfits, inbright='')
        
        tb.open('gen/gen.alma.out10.skymodel')
        self.assertTrue(np.amax(tb.getcol('map')) == 0.00049030134687200189)
        tb.close()
        
    def test_indirection(self):
        '''
            test_indirection
            -------------------
            
            Test that this parameter gives the central direction to place the sky model image
            " to use the whatever is in the image already
        '''
        casalog.setlogfile('testlog.log')
        
        simobserve(project='gen', skymodel=genfits, indirection='J2000 19h00m00 -40d00m00')
        self.assertTrue('J2000 19h00m00 -40d00m00' in open('testlog.log').read())
        casalog.setlogfile(logpath)
        
        # now test the default
        
        casalog.setlogfile('testlog.log')
        
        simobserve(project='gen', skymodel=genfits, indirection='')
        self.assertTrue(direction in open('testlog.log').read())
        casalog.setlogfile(logpath)
        
    def test_incell(self):
        '''
            test_incell
            ---------------
            
            Test that this parameter sets a new cell/pixel size
            
            This result should be in unit of arcseconds
        '''
        
        simobserve(project='gen', skymodel=genfits, incell='.13arcsec')
        
        casalog.setlogfile('testlog.log')
        imhead('gen/gen.alma.out10.skymodel', verbose=True)
        self.assertTrue('1.300000e-01 arcsec' in open('testlog.log').read())
        casalog.setlogfile(logpath)
        
        # Check default inputs
        
        simobserve(project='gen', skymodel=genfits, incell='')
        
        casalog.setlogfile('testlog.log')
        imhead('gen/gen.alma.out10.skymodel', verbose=True)
        self.assertTrue('1.000000e-01 arcsec' in open('testlog.log').read())
        casalog.setlogfile(logpath)
        
    def test_incenter(self):
        '''
            test_incenter
            ---------------
            
            Test that this parameter sets the center channel
            Set new frequency given in GHz. they are returned in Hz
        '''
        
        simobserve(project='gen', skymodel=genfits, incenter='85GHz')
        
        ms.open('gen/gen.alma.out10.ms')
        
        reffreq = ms.getspectralwindowinfo()['0']['RefFreq']
        self.assertTrue(reffreq == 85000000000.0)
        ms.done()
        
    def test_inwidth(self):
        '''
            test_inwidth
            ---------------
            
            Test that the inwidth paramter sets a new channel width for the generated ms
            Values are given in casa in Hz
        '''
        
        simobserve(project='gen', skymode=genfits, inwidth='10MHz')
        
        ms.open('gen/gen.alma.out10.ms')
        chanwidth = ms.getspectralwindowinfo()['0']['ChanWidth']
        self.assertTrue(chanwidth == 10000000.0)
        ms.done()
        
    def test_complist(self):
        '''
            WARNING this task does not currently handle component lists correctly
            For now I will leave this one blank
        '''
        
    def test_compwidth(self):
        '''
            WARNING Complist is not currently being handled properly
                    This will be blank until complist is working as intended
        '''
        
    def test_setpointingsint(self):
        '''
            test_setpointingsint
            -----------------------
            
            Test that the setpointings integration sub parameter changes the integration time on the simulation
            
            Also test the changing set pointings to False makes the sub parameter have no effect
        
        '''
        
        simobserve(project='gen', skymode=genfits, setpointings=True, integration='20s')
        
        ms.open('gen/gen.alma.out10.ms')
        integrationTime = ms.summary()['scan_1']['0']['IntegrationTime']
        ms.done()
        self.assertTrue(integrationTime == 20.0)
        
        shutil.rmtree('gen')
        
        simobserve(project='gen', skymode=genfits, setpointings=False, integration='20s')
        
        ms.open('gen/gen.alma.out10.ms')
        integrationTime = ms.summary()['scan_1']['0']['IntegrationTime']
        ms.done()
        self.assertFalse(integrationTime == 20.0)
        
    def test_setpointingdirection(self):
        '''
            test_setpointingdirection
            -------------------------------
            
            Test that the direction subparameter will change the mosaic center direction
            
            If unset simobserve will use the center of the skymodel image
        '''
        
        simobserve(project='gen', skymode=genfits, setpointings=True, direction='J2000 10h00m00 -30d00m10')
        
        ms.open('gen/gen.alma.out10.ms')
        centerdec = ms.summary()['field_3']['direction']['m1']['value']
        decval = -0.5236472569664098
        ms.done()
        
        self.assertTrue(centerdec = -0.5236472569664098)
        
    def test_setpointingmapsize(self):
        '''
            test_setpointingmapsize
            --------------------------
                
            Test that the mapsize subparameter will change the angular size of the mosaic
            
            Set to "" to cover the model image
            
            NOTE I'm not sure where this information is stored, sdantlist somehow gets this information (25.6 arcsec)?
        '''
        
    def test_setpointingmaptype(self):
        '''
            test_setpointingmaptype
            --------------------------
            
            Test that this subparameter changes how the pointings are calculated for mosaic observation
            
            NOTE I'm also not sure how to confirm which type was used, or what the differeces would be here
        '''
        
        
        
    def test_pointingspacing(self):
        '''
            test_pointingspacing
            ---------------------
            
            Test that this parameter sets the spacing between the primary beams
        '''
        
        
    def test_ptgfile(self):
        '''
            test_ptgfile
            --------------
            
            Test that simobserve takes a provided pointing file
            
            If time is not provided in the pointing file then the integration time is used in its place
        
        '''
        
        simobserve(project='gen', skymode=genfits, setpointings=False, ptgfile='ptg.txt')
        
        self.assertTrue(filecmp('ptg.txt', 'gen/gen.alma.out10.ptg.txt', shallow=False))
    
    def test_obsmodeint(self):
        '''
            test_obsmodeint
            ---------------------
            
            Test that the int parameter runs and then check that the observation mode used when calculating was the interferonmeter mode
        '''
        
        self.assertTrue(simobserve(project='gen', skymode=genfits, obsmode='int'))
        # Where can I find the observation mode used when calculating
        self.assertTrue(simobserve(project='gen', skymode=genfits, obsmode='sd'))
        
    def test_obsmodetime(self):
        '''
            test_obsmodetime
            ------------------
            
            WARNING the date is visible in listobs, but its one day at a time
        '''
        
        
        simobserve(project='gen', skymode=genfits, obsmode='int', refdate='2011/08/21')
        
        casalog.setlogfile('testlog.log')
        ms.open('gen/gen.alma.out10.ms/')
        ms.summary()
        ms.close()
        
        self.assertTrue('21-Aug-2011' in open('testlog.log').read())
        
        casalog.setlogfile(logpath)
        
        # Now test the single dish mode
        
        os.remove('testlog.log')
    
        simobserve(project='gen', skymode=genfits, obsmode='sd', refdate='2011/08/21')
        
        casalog.setlogfile('testlog.log')
        ms.open('gen/gen.alma.out10.ms/')
        ms.summary()
        ms.close()
        
        self.assertTrue('21-Aug-2011' in open('testlog.log').read())
        
        casalog.setlogfile(logpath)
        
    def test_obsmodehourangle(self):
        '''
            test_obsmodehourangle
            ------------------------
            
            Check that this paramter changes the hour angle of the observation. If a unit is not provided it will assume hours
            
            'transit' is the same as 0h
            
            WARNING observes up until 22 min before the given hour angle ? I'm not sure what the cause of this is
        '''
        
        
        simobserve(project='gen', skymode=genfits, obsmode='int', hourangle='17:00:00')
        
        casalog.setlogfile('testlog.log')
        ms.open('gen/gen.alma.out10.ms/')
        ms.summary()
        ms.close()
        
        self.assertTrue('17:00:00' in open('testlog.log').read())
        
        casalog.setlogfile(logpath)
        
    def test_obsmodetotaltime(self):
        '''
            test_obsmodetotaltime
            -----------------------
            
            Test that the total elapsed time is changed by the paramter of totaltime
        '''
        
        simobserve(project='gen', skymode=genfits, obsmode='int', totaltime='7500s')
        
        ms.open('gen/gen.alma.out10.ms/')
        elapsedtime = ms.summary()['IntegrationTime']
        ms.close()
        
        self.assertTrue(elapsedtime == 7500.0)
        
    def test_obsantennalist(self):
        '''
            test_obsantennalist
            ---------------------
            
            Test that simobserve takes an ASCII file containing the antenna positions
            
            if antennalist='' simobserve will not produce an interferometric MS
            
            also takes alma;0.5arcsec to be full 12m ALMA configuration
        '''
        
        runResult = simobserve(project='gen', skymode=genfits, obsmode='int', antennalist='alma;0.5arcsec')
        
        self.assertTrue(runResult)
        
        runResult = simobserve(project='gen', skymode=genfits, obsmode='int', antennalist='ASCIIlist.txt')
        
        self.assertTrue(runResult)
        
    def test_obscaldirection(self):
        '''
            test_obscaldirection
            -----------------------
            
            takes a point source calibrator
            
            NOTE this is an experimental parameter. What is this supposed to be? I need an example of what the parameter should be
        '''
        
        
        
        
    def test_obscalflux(self):
        '''
            test_obscalflux
            ------------------
            
            Test that this subparameter changes the flux density of the calibrator
            
            Default is calflux='1Jy'
            
            NOTE I also don't know where to find this infromation. Come back to this one later
        '''
        
    def test_obssdantlist(self):
        '''
            test_obssdantlist
            --------------
            
            Test that this parameter takes a single dish antenna position file
            
            Antenna list default is sdantlist='aca.tp.cfg'
        '''
        
        runResult = simobserve(project='gen', skymode=genfits, obsmode='sd', sdantlist='sdantlist.cfg')
        
        self.assertTrue(runResult)
        
        
        
    def test_obssdant(self):
        '''
            test_obssdantlist
            --------------
            
            Test that this sub-parameter takes the index of the antenna to use for total power
            
            defaults to first antenna sdant=0
            
            This requires the use of and sdantlist
            
            NOTE Come back to this one
        '''
        
        runResult = simobserve(project='gen', skymode=genfits, obsmode='sd', sdantlist='sdantlist.cfg', sdant=1)
        self.assertTrue(runResult)
        
        runResult = simobserve(project='gen', skymode=genfits, obsmode='sd', sdantlist='sdantlist.cfg', sdant=10000)
        self.assertTrue(runResult != True)
        
        
    def test_thermalnoise(self):
        '''
            test_thermalnoise
            -------------------
            
            Test that thermal noise is added to the data
            
            Can contruct a profile for the ALMA site or a manual option can be used
            
            Two parameters (tsys-atm, tsys-manual)not including the default ''
            
            NOTE It would be great to have some examples of this
        '''
        
        runResult = simobserve(project='gen', skymode=genfits, thermalnoise='tsys-atm')
        self.assertTrue(runResult)
        
        runResult = simobserve(project='gen', skymode=genfits, thermalnoise='tsys-manual')
        self.assertTrue(runResult)
        
        
        
    def test_tground(self):
        '''
            test_tground
            --------------
            
            Test that this parameter changes the ambient ground/spillover temperature in K
            
            This can take an int or float
            
            NOTE come back to this one
        '''
        
        runResult = simobserve(project='gen', skymode=genfits, thermalnoise='tsys-atm', t_ground=268)
        self.assertTrue(runResult)
        
        runResult = simobserve(project='gen', skymode=genfits, thermalnoise='tsys-atm', t_ground=268.00)
        self.assertTrue(runResult)
        
        
        
    def test_seed(self):
        '''
            test_seed
            -----------
            
            This parameter provides the seen for the noise generation
            
            This parameter takes an int
            
            NOTE I will have to come back to almost all of these later
        '''
        
        runResult = simobserve(project='gen', skymode=genfits, thermalnoise='tsys-atm', seed=12345)
        self.assertTrue(runResult)
        
        
        
    
    def test_userpwv(self):
        '''
            test_userpwv
            --------------
            
            Test that this parameter sets the precipitable water vapor level at zenith
            
            Only used if constructing at atmospheric model and in units of mm
        '''
        
        runResult = simobserve(project='gen', skymode=genfits, thermalnoise='tsys-atm', user_pwv=0.7)
        self.assertTrue(runResult)
        
        
    def test_tsky(self):
        '''
            test_tsky
            -----------
            
            Test that this parameter sets the atmospheric temperature in K
        '''
        
        runResult = simobserve(project='gen', skymode=genfits, thermalnoise='tsys-manual', t_sky='286')
        self.assertTrue(runResult)
        
        
    def test_tau0(self):
        '''
            test_tau0
            -----------
            
            Test that this parameter changes the zenith opacity at the observing frequency
        '''
        
        runResult = simobserve(project='gen', skymode=genfits, thermalnoise='tsys-manual', tau0=0.1)
        self.assertTrue(runResult)
        
    def test_leakage(self):
        '''
            test_leakage
            --------------
            
            Test that this parameter adds cross polarization curruption of some fractional magnitude
            
            WARNING it seems like this value can only be 0.0 (?)
            
            This parameter might be broken right now
        '''
        
        self.Fail()
        
        
        
    def test_graphics(self):
        '''
            test_graphics
            ---------------
            
            Test that this parameter changes if graphics are displayed or if they are saved to a file
            
            NOTE Need a way to check if the graphic window was opened
        '''
        
        simobserve(project='gen', skymode=genfits, graphics='screen')
        self.assertFalse(os.path.exists('gen/gen.alma.out10.observe.png'))
        self.assertFalse(os.path.exists('gen/gen.alma.out10.skymodel.png'))
        
        shutil.rmtree('gen')
        
        simobserve(project='gen', skymode=genfits, graphics='file')
        self.assertTrue(os.path.exists('gen/gen.alma.out10.observe.png'))
        self.assertTrue(os.path.exists('gen/gen.alma.out10.skymodel.png'))
        
        shutil.rmtree('gen')
        
        simobserve(project='gen', skymode=genfits, graphics='both')
        self.assertTrue(os.path.exists('gen/gen.alma.out10.observe.png'))
        self.assertTrue(os.path.exists('gen/gen.alma.out10.skymodel.png'))
        
        shutil.rmtree('gen')
        
        simobserve(project='gen', skymode=genfits, graphics='none')
        self.assertFalse(os.path.exists('gen/gen.alma.out10.observe.png'))
        self.assertFalse(os.path.exists('gen/gen.alma.out10.skymodel.png'))
        
        
        
    def test_verbose(self):
        '''
            test_verbose
            --------------
            
            Test that this changes the amount of information displayed in the logger and terminal
        '''
        casalog.setlogfile('testlog.log')
        
        simobserve(project='gen', skymode=genfits, verbose=False)
        noVerb = os.path.getsize('testlog.log')
        os.remove('testlog.log')
        casalog.setlogfile('testlog.log')
        
        simobserve(project='gen', skymode=genfits, verbose=True)
        Verb = os.path.getsize('testlog.log')
        
        self.assertTrue(Verb > noVerb)
        
        
    def test_overwrite(self):
        '''
            test_overwrite
            ----------------
            
            Test that the this parameter allows existing project directories to be overwritten
            
            Default is to not allow overwriting
        '''
        
        simobserve(project='gen', skymode=genfits)
        self.assertTrue(simobserve(project='gen', skymode=genfits, overwrite=True))
        self.assertTrue(simobserve(project='gen', skymode=genfits, overwrite=False) != True)
        
    
def suite():
    return[simobserve_test]

if __name__ == '__main__':
    unittest.main()
