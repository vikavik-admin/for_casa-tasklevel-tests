##########################################################################
# test_req_task_listcal.py
#
# Copyright (C) 2018
# Associated Universities, Inc. Washington DC, USA.
#
# This script is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# [Add the link to the JIRA ticket here once it exists]
#
# Based on the requirements listed in plone found here:
# https://casa.nrao.edu/casadocs-devel/stable/global-task-list/task_listcal/about
#
#
##########################################################################

CASA6 = False
try:
    import casatools
    from casatasks import listcal, casalog
    CASA6 = True
except ImportError:
    from __main__ import default
    from tasks import *
    from taskinit import *
import sys
import os
import unittest
import shutil
import numpy as np
import subprocess

### DATA ###

if CASA6:
    datapath = casatools.ctsys.resolve('visibilities/vla/ngc5921.ms/')
    calpath = casatools.ctsys.resolve('caltables/ngc5921.ref1a.gcal/')
    
else:
    if os.path.exists(os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req'):
        datapath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/vla/ngc5921.ms/'
        calpath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/caltables/ngc5921.ref1a.gcal/'
        
    else:
        datapath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/vla/ngc5921.ms/'
        calpath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/caltables/ngc5921.ref1a.gcal/'
        
        
textout = 'testing.txt'
        
def selectLine(sel):
    with open(textout,'r') as f:
        i = 0
        for line in f:
            if i==sel:
                selected = line
            i+=1
    return selected

def change_perms(path):
    os.chmod(path, 0o777)
    for root, dirs, files in os.walk(path):
        for d in dirs:
            os.chmod(os.path.join(root,d), 0o777)
        for f in files:
            os.chmod(os.path.join(root,f), 0o777)


logpath = casalog.logfile()
testlog = 'testlog.log'

genscript = 'generated.py'

datacopy = 'datacopy.ms'
calcopy = 'calcopy.gcal'
        
class listcal_test(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        
        # not sure if I should do this
        os.system('mkdir fake.dir')

    def setUp(self):
        
        if not CASA6:
            default(listcal)
            
        shutil.copytree(datapath, datacopy)
        shutil.copytree(calpath, calcopy)
        
        change_perms(datacopy)
        change_perms(calcopy)
    
    def tearDown(self):
        
        casalog.setlogfile(logpath)
        
        if os.path.exists(datacopy):
            shutil.rmtree(datacopy)
            
        if os.path.exists(calcopy):
            shutil.rmtree(calcopy)
        
        if os.path.exists(textout):
            os.remove(textout)
        
        if os.path.exists(testlog):
            os.remove(testlog)
            
        if os.path.exists('generated.py'):
            os.remove('generated.py')
            
        #if os.path.exists(genscript):
            #os.remove(genscript)
    
    @classmethod
    def tearDownClass(cls):
        shutil.rmtree('fake.dir')
    
    def test_input(self):
        '''
            test_input
            ------------
            
            Check that listcal takes a valid ms and caltable, and that an invalid ms and caltable is rejected
        '''
        
        casalog.setlogfile(testlog)
        
        listcal(vis=datacopy, caltable=calcopy)
        self.assertFalse('SEVERE' in open(testlog).read())
        
        listcal(vis='fake.dir', caltable=calcopy)
        self.assertTrue('SEVERE' in open(testlog).read())
        
        
        
    def test_listfile(self):
        '''
            test_listfile
            ---------------
            
            Check that listfile specifes an output file for the task output
        '''
        
        listcal(vis=datacopy, caltable=calcopy, listfile=textout)
        self.assertTrue(os.path.exists(textout))
        
        
    def test_topHeader(self):
        '''
            test_topHeader
            ----------------
            
            Check that the top header contains the correct information. (SpwID, Date, CalTable, and MS name)
        '''
        
        headerItems = ['SpwID', 'Date', 'CalTable', 'MS name']
        listcal(vis=datacopy, caltable=calcopy, listfile=textout)
        header = selectLine(0)
        print(header)
        
        for item in headerItems:
            self.assertTrue(item in header)
            
            
    def test_lowHeader(self):
        '''
            test_lowHeader
            ----------------
            
            Check that the lower header contains the correct information. (Ant, Time, Field, Chn, Amp, Phs, F)
        '''
        
        headerItems = ['Ant', 'Time', 'Field', 'Chn', 'Amp', 'Phs', 'F']
        listcal(vis=datacopy, caltable=calcopy, listfile=textout)
        header = selectLine(2) + selectLine(3)
        
        for item in headerItems:
            self.assertTrue(item in header)
            
    
    def test_fieldSelect(self):
        '''
            test_fieldSelect
            ------------------
            
            Check that the field selection parameter properly selects a field
        '''
        
        listcal(vis=datacopy, caltable=calcopy, listfile=textout)
        fieldName = selectLine(5).split(' ')[1]
        
        self.assertTrue(fieldName == '1331+30500002_0')
        
        
    def test_antennaSelect(self):
        '''
            test_antennaSelect
            ---------------------
            
            Check that the antenna selection parameter properly selects a field
        '''
        
        listcal(vis=datacopy, caltable=calcopy, listfile=textout, antenna='0')
        antennaName = selectLine(2).split()[3]
        
        self.assertTrue(antennaName == 'VA01')
        
        
    def test_spwSelect(self):
        '''
            test_spwSelect
            ----------------
            
            Check that the spw selection parameter properly selects a spw
        '''
        
        listcal(vis=datacopy, caltable=calcopy, listfile=textout, spw='0')
        SpwID = selectLine(0).split()[2][0]
        
        self.assertTrue(SpwID == '0')
        
        casalog.setlogfile(testlog)
        listcal(vis=datacopy, caltable=calcopy, listfile=textout, spw='1')
        
        self.assertTrue('SEVERE' in open(testlog).read())
        
    
def suite():
    return[listcal_test]

if __name__ == '__main__':
    unittest.main()
