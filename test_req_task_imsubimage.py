##########################################################################
# test_req_task_imsubimage.py
#
# Copyright (C) 2018
# Associated Universities, Inc. Washington DC, USA.
#
# This script is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# [Add the link to the JIRA ticket here once it exists]
#
# Based on the requirements listed in plone found here:
# https://casa.nrao.edu/casadocs-devel/stable/global-task-list/task_imsubimage/about
#
#
##########################################################################

CASA6 = False
try:
    import casatools
    from casatasks import imsubimage, casalog
    CASA6 = True
except ImportError:
    from __main__ import default
    from tasks import *
    from taskinit import *
import sys
import os
import unittest
import shutil
import numpy as np

### DATA ###

if CASA6:
    stokespath = casatools.ctsys.resolve('image/image_input_processor.im/')
    tb = casatools.table()
    ia = casatools.image()

else:
    if os.path.exists(os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req'):
        stokespath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/image/image_input_processor.im/'
        
    else:
        stokespath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/image/image_input_processor.im/'
        
def makeImage():
    
    imagename = "gen.im"
    ia.fromshape(imagename, [10, 10, 10])
    bb = ia.getchunk()
    for i in range(10):
        bb[i,5,:] = i
        bb[i,0:5,:] = i+1
        bb[i,6:10,:] = i+2
    ia.putchunk(bb)
    
    ia.done()
        
    return imagename

def makeDegImage():
    
    imagename = "gendeg.im"
    ia.fromshape(imagename, [10, 10, 1, 1])
    bb = ia.getchunk()
    for i in range(10):
        bb[i,5,:] = i
        bb[i,0:5,:] = i+1
        bb[i,6:10,:] = i+2
    ia.putchunk(bb)
    
    ia.done()

def makeFloatImage():
    
    imagename = "genfloat.im"
    ia.fromshape(imagename, [10, 10, 10])
    bb = ia.getchunk()
    for i in range(10):
        bb[i,5,:] = i+.3
        bb[i,0:5,:] = i+1.3
        bb[i,6:10,:] = i+2.3
    ia.putchunk(bb)
    
    ia.done()
        
    return imagename

def makeCompImage():
    
    imagename = "gencomp.im"
    putArr = np.array([[complex(j,2) for i in range(10)] for j in range(10)])
    ia.fromarray(outfile=imagename, pixels=putArr)
    
    ia.done()
        
    return imagename

useImage = 'gen.im'
useFloat = 'genfloat.im'
useComp = 'gencomp.im'
useDeg = 'gendeg.im'

subImage = 'subim.im'
subImage2 = 'subim2.im'
arrayIm = 'arrayIm.im'

logpath = casalog.logfile()
testlog = 'testlog.log'

class imsubimage_test(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        
        makeImage()
        makeFloatImage()
        makeCompImage()
        makeDegImage()
    
    def setUp(self):
        if not CASA6:
            default(imsubimage)
    
    def tearDown(self):
        casalog.setlogfile(logpath)
        
        if os.path.exists(testlog):
            os.remove(testlog)
            
        if os.path.exists(subImage):
            shutil.rmtree(subImage)
            
        if os.path.exists(subImage2):
            shutil.rmtree(subImage2)
            
        if os.path.exists(arrayIm):
            shutil.rmtree(arrayIm)
    
    @classmethod
    def tearDownClass(cls):
        
        shutil.rmtree(useImage)
        shutil.rmtree(useFloat)
        shutil.rmtree(useComp)
        shutil.rmtree(useDeg)
        
    def test_makesImage(self):
        '''
            test_makesImage
            -----------------
            
            Checks that all or part of an image is copied into a new image
        '''
        
        imsubimage(imagename=useImage, outfile=subImage)
        self.assertTrue(os.path.exists(subImage))
        
    def test_floatValue(self):
        '''
            test_floatValue
            -----------------
            
            Check that the task supports images with float values
        '''
        
        self.assertTrue(imsubimage(imagename=useFloat, outfile=subImage))
        
        
    def test_floatComp(self):
        '''
            test_floatValue
            -----------------
            
            Check that the task supports images with complex values
        '''
        
        self.assertTrue(imsubimage(imagename=useComp, outfile=subImage))
        
    def test_dropdeg(self):
        '''
            test_dropdeg
            --------------
            
            Check that degenerate axis can be dropped with dropdeg = True
        '''
        
        imsubimage(imagename=useDeg, outfile=subImage, dropdeg= False)
        imsubimage(imagename=useDeg, outfile=subImage2, dropdeg = True)
        
        tb.open(subImage)
        noDrop = np.shape(tb.getcol('map'))
        tb.close()
        
        tb.open(subImage2)
        withDrop = np.shape(tb.getcol('map'))
        tb.close()
        
        self.assertTrue(noDrop != withDrop)
        
    def test_outMask(self):
        '''
            test_outMask
            --------------
            
            Check that the image is masked properly
            NOTE: What is the OTF mask
        '''
        
        imsubimage(imagename=useImage, outfile=subImage, mask='gen.im>2')
        
        self.assertTrue(os.path.exists('subim.im/mask0'))
        
        
    def test_missingDim(self):
        '''
            test_missingDim
            -----------------
            
            Check that if a mask with fewer dimensions than the image is provided then missing dimensions will be added
        '''
        
        ia.fromshape(arrayIm, [10,10])
        ia.done()
        
        self.assertTrue(imsubimage(imagename=useImage, outfile=subImage, mask=arrayIm))
        
    def test_stretch(self):
        '''
            test_stretch
            --------------
            
            Check that the mask axes are stretched if stretch = True
        '''
        
        ia.fromshape(arrayIm, [1,10,10])
        ia.done()

        self.assertTrue(imsubimage(imagename=useImage, outfile=subImage, mask=arrayIm, stretch=True))
        
    def test_verbose(self):
        '''
            test_verbose
            --------------
            
            Check that the setting verbose = True posts additional information to the logger
        '''
        
        casalog.setlogfile(testlog)
        imsubimage(imagename=useImage, outfile=subImage, verbose=False)
        nonVerb = os.path.getsize(testlog)
        casalog.setlogfile(logpath)
        os.remove(testlog)
        
        casalog.setlogfile(testlog)
        imsubimage(imagename=useImage, outfile=subImage2, verbose=True)
        withVerb = os.path.getsize(testlog)
        
        self.assertTrue(withVerb > nonVerb)
        
    def test_keepAxes(self):
        '''
            test_keepAxes
            ---------------
        
            Check that this parameter selects the degenerate axes to keep
        '''
        
        imsubimage(imagename=useDeg, outfile=subImage, dropdeg=True)
        imsubimage(imagename=useDeg, outfile=subImage2, dropdeg=True, keepaxes=[3])
        
        tb.open(subImage)
        noAxesKeep = np.shape(tb.getcol('map'))
        tb.close()
        
        tb.open(subImage2)
        withAxesKeep = np.shape(tb.getcol('map'))
        tb.close()
        
        self.assertFalse(noAxesKeep == withAxesKeep)
        
    def test_box(self):
        '''
            test_box
            ----------
            
            Check that the box parameter selects a subset of the image
        '''
        
        imsubimage(imagename=useImage, outfile=subImage, box='0,0,3,3')
        
        tb.open(subImage)
        withBox = np.shape(tb.getcol('map'))
        tb.close()
        
        tb.open(useImage)
        noBox = np.shape(tb.getcol('map'))
        tb.close()
        
        self.assertFalse(withBox == noBox)
        
    def test_region(self):
        '''
            test_region
            -------------
            
            Check that the region selection selects a subset of the image
        '''
        
        imsubimage(imagename=useImage, outfile=subImage, region='centerbox[[5pix,5pix],[3pix,3pix]]')
        
        tb.open(subImage)
        withRegion = np.shape(tb.getcol('map'))
        tb.close()
        
        tb.open(useImage)
        noRegion = np.shape(tb.getcol('map'))
        tb.close()
        
        self.assertFalse(withRegion == noRegion)
        
    def test_chans(self):
        '''
            test_chans
            ------------
            
            Check that the chans parameter selects a subset of the image
        '''
        
        imsubimage(imagename=useImage, outfile=subImage, chans='1')
        
        tb.open(subImage)
        withChan = np.shape(tb.getcol('map'))
        tb.close()
        
        tb.open(useImage)
        noChan = np.shape(tb.getcol('map'))
        tb.close()
        
        self.assertFalse(withChan == noChan)
        
    
    def test_stokes(self):
        '''
            test_stokes
            -------------
            
            Check that the stokes selection parameter properly selects a subset of the data
        '''
        
        imsubimage(imagename=stokespath, outfile=subImage, stokes='i')
        
        tb.open(subImage)
        outSize = np.shape(tb.getcol('map'))
        tb.close()
    
        self.assertTrue(outSize[3] == 1)
        
    def test_overwrite(self):
        '''
            test_overwrite
            ----------------
            
            Check that overwrite = True is required to overwrite the output file
        '''
        
        imsubimage(imagename=useImage, outfile=subImage)
        if CASA6:
            with self.assertRaises(RuntimeError):
                imsubimage(imagename=useImage, outfile=subImage)
        else:
            casalog.setlogfile(testlog)
            imsubimage(imagename=useImage, outfile=subImage)
            self.assertTrue('SEVERE' in open(testlog).read())
            
        self.assertTrue(imsubimage(imagename=useImage, outfile=subImage, overwrite=True))
        
        
        
def suite():
    return[imsubimage_test]


if __name__ == '__main__':
    unittest.main()
