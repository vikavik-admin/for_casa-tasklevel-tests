########################################################################
# test_req_task_imtrans.py
#
# Copyright (C) 2018
# Associated Universities, Inc. Washington DC, USA
#
# This script is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# [Add the link to the JIRA ticket here once it exists]
#
# Based on the requirements listed in plone found here:
# https://casa.nrao.edu/casadocs-devel/stable/global-task-list/task_imtrans/about
#
#
##########################################################################

CASA6 = False
try:
    import casatools
    from casatasks import imtrans, casalog, imhead
    CASA6 = True
except ImportError:
    from __main__ import default
    from tasks import *
    from taskinit import *
import sys
import os
import unittest
import shutil

if CASA6:
    casaimagepath = casatools.ctsys.resolve('image/ngc5921.clean.image')
    fitspath = casatools.ctsys.resolve('fits/1904-66_AIR.fits')
else:
    if os.path.exists(os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req'):
        dataroot = os.environ.get('CASAPATH').split()[0] + '/'
        casaimagepath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/image/ngc5921.clean.image'
        fitspath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/fits/1904-66_AIR.fits'
    else:
        dataroot = os.environ.get('CASAPATH').split()[0] + '/'
        casaimagepath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/image/ngc5921.clean.image'
        fitspath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/fits/1904-66_AIR.fits'
        
logpath = casalog.logfile()

class imtrans_test(unittest.TestCase):
    
    def setUp(self):
        if not CASA6:
            default(imtrans)
        else:
            pass
            
    def tearDown(self):
        casalog.setlogfile(logpath)
        if os.path.exists('testlog.log'):
            os.remove('testlog.log')
            
    def test_opensImageFile(self):
        ''' 1. test_opensImageFile: Check that imtrans opens an image file '''
        success = imtrans(imagename=casaimagepath, outfile="aftertrans.im", order = 1230)
        shutil.rmtree("aftertrans.im")
        self.assertTrue(success)
            
    def test_opensFITSFile(self):
        ''' 2. test_opensFITSFile: Check that imtrans opens a FITS file '''
        success = imtrans(imagename=fitspath, outfile="aftertrans.im", order = 10) 
        shutil.rmtree("aftertrans.im")
        self.assertTrue(success)   
        
    def test_specifyAxesUsingInteger(self):
        ''' 3. test_specifyAxesUsingInteger: Check that imtrans takes an ordering expressed as
        an integer '''
        beforeimtrans = imhead(casaimagepath)
        axesbeforeimtrans = beforeimtrans['axisnames']
        imtrans(imagename=casaimagepath, outfile="aftertrans.im", order = 1230)
        afterimtrans = imhead("aftertrans.im")
        axesafterimtrans = afterimtrans['axisnames']
        neworder = [1, 2, 3, 0]
        reorderedlist = [axesbeforeimtrans[i] for i in neworder]
        shutil.rmtree('aftertrans.im')
        self.assertTrue((axesafterimtrans == reorderedlist).all())
        
    def test_specifyAxesUsingString(self):
        ''' 4. test_specifyAxesUsingStrings: Check that imtrans takes an ordering expressed as
        a string '''
        beforeimtrans = imhead(casaimagepath)
        axesbeforeimtrans = beforeimtrans['axisnames']
        imtrans(imagename=casaimagepath, outfile="aftertrans.im", order = "1230")
        afterimtrans = imhead("aftertrans.im")
        axesafterimtrans = afterimtrans['axisnames']
        neworder = [1, 2, 3, 0]
        reorderedlist = [axesbeforeimtrans[i] for i in neworder]
        shutil.rmtree('aftertrans.im')
        self.assertTrue((axesafterimtrans == reorderedlist).all())
        
    def test_specifyAxesUsingCharacters(self):
        ''' 5. test_specifyAxesUsingCharacters: Check that imtrans takes an ordering expressed
        as a list of characters '''
        beforeimtrans = imhead(casaimagepath)
        axesbeforeimtrans = beforeimtrans['axisnames']
        imtrans(imagename=casaimagepath, outfile="aftertrans.im", order = ['d', 's', 'f', 'r'])
        afterimtrans = imhead("aftertrans.im")
        axesafterimtrans = afterimtrans['axisnames']
        neworder = [1, 2, 3, 0]
        reorderedlist = [axesbeforeimtrans[i] for i in neworder]
        shutil.rmtree('aftertrans.im')
        self.assertTrue((axesafterimtrans == reorderedlist).all())
        
    def test_specifyAxesUsingStrings(self):
        ''' 6. test_specifyAxesUsingStrings: Check that imtrans takes an ordering expressed 
        as a list of strings '''
        beforeimtrans = imhead(casaimagepath)
        axesbeforeimtrans = beforeimtrans['axisnames']
        imtrans(imagename=casaimagepath, outfile="aftertrans.im", order = ['decli', 'stok', 'frequ', 'right'])
        afterimtrans = imhead("aftertrans.im")
        axesafterimtrans = afterimtrans['axisnames']
        neworder = [1, 2, 3, 0]        
        reorderedlist = [axesbeforeimtrans[i] for i in neworder]
        shutil.rmtree('aftertrans.im')
        self.assertTrue((axesafterimtrans == reorderedlist).all())
        
def suite():
    return[imtrans_test]
        
# Main #
if __name__ == '__main__':
    unittest.main()