##########################################################################
# test_req_task_gencal.py
#
# Copyright (C) 2018
# Associated Universities, Inc. Washington DC, USA.
#
# This script is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# [Add the link to the JIRA ticket here once it exists]
#
# Based on the requirements listed in plone found here:
# https://casa.nrao.edu/casadocs-devel/stable/global-task-list/task_gencal/about
#
#
##########################################################################

CASA6 = False
try:
    import casatools
    from casatasks import gencal
    CASA6 = True
    tb = casatools.table()
except ImportError:
    from __main__ import default
    from tasks import *
    from taskinit import *
import sys
import os
import unittest
import shutil
import numpy as np

### DATA ###

if CASA6:
    datapath = casatools.ctsys.resolve('visibilities/vla/gaincaltest2.ms')
    refpath = casatools.ctsys.resolve('visibilities/alma/nep2-shrunk.ms')
    tsyspath = casatools.ctsys.resolve('visibilities/alma/uid___A002_X30a93d_X43e_small.ms/')
    vlapath = casatools.ctsys.resolve('visibilities/evla/CAS-6733.ms')
    ngcpath = casatools.ctsys.resolve('visibilities/vla/ngc5921.ms')
    imref = casatools.ctsys.resolve('image/ngc5921.clean.image/')

else:
    if os.path.exists(os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req'):
        datapath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/vla/gaincaltest2.ms'
        refpath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/alma/nep2-shrunk.ms'
        tsyspath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/alma/uid___A002_X30a93d_X43e_small.ms/'
        vlapath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/evla/CAS-6733.ms/'
        ngcpath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/vla/ngc5921.ms'
        imref = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/image/ngc5921.clean.image/'
        
    else:
        datapath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/vla/gaincaltest2.ms'
        refpath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/alma/nep2-shrunk.ms'
        tsyspath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/alma/uid___A002_X30a93d_X43e_small.ms/'
        vlapath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/evla/CAS-6733.ms/'
        ngcpath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/vla/ngc5921.ms'
        imref = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/image/ngc5921.clean.image/'
        

def getParam(data, col='CPARAM'):
    
    tb.open(data)
    ans = tb.getcol(col)
    tb.close()
    
    return ans


def getParamMean(data, col='FPARAM'):
    
    tb.open(data)
    ans = tb.getcol(col)
    tb.close()
    ansmean = np.mean(ans)
    
    return ansmean
    
def change_perms(path):
    os.chmod(path, 0o777)
    for root, dirs, files in os.walk(path):
        for d in dirs:
            os.chmod(os.path.join(root,d), 0o777)
        for f in files:
            os.chmod(os.path.join(root,f), 0o777)
            

datacopy = 'datacopy.ms'
refcopy = 'refcopy.ms'
tsyscopy = 'tsyscopy.ms'
vlacopy = 'vlacopy.ms'
ngccopy = 'ngccopy.ms'
        
testcal = 'testcal.cal'
testcal2 = 'testcal2.cal'

class gencal_test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        shutil.copytree(datapath, datacopy)
        shutil.copytree(refpath, refcopy)
        shutil.copytree(tsyspath, tsyscopy)
        shutil.copytree(vlapath, vlacopy)
        shutil.copytree(ngcpath, ngccopy)
        
        change_perms(datacopy)
        change_perms(refcopy)
        change_perms(tsyscopy)
        change_perms(vlacopy)
        change_perms(ngccopy)
        
    def setUp(self):
        if not CASA6:
            default(gencal)
        
    def tearDown(self):
        if os.path.exists(testcal):
            shutil.rmtree(testcal)
            
        if os.path.exists(testcal2):
            shutil.rmtree(testcal2)
        
    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(datacopy)
        shutil.rmtree(refcopy)
        shutil.rmtree(tsyscopy)
        shutil.rmtree(vlacopy)
        shutil.rmtree(ngccopy)
        
        
        
    def test_caltable(self):
        '''
            test_caltable
            ------------------------
        '''
        
        gencal(datacopy, caltable=testcal, caltype='amp', parameter=[1])
        self.assertTrue(os.path.exists(testcal))
        
        
    def test_caltypeph(self):
        '''
            test_caltypeph
            ------------------------
        '''
        
        gencal(datacopy, caltable=testcal, caltype='amp', parameter=[1])
        gencal(datacopy, caltable=testcal2, caltype='ph', parameter=[1])
        
        res1 = getParam(testcal)
        res2 = getParam(testcal2)

        self.assertFalse(np.all(res1 == res2))
        
        
    def test_caltypesbd(self):
        '''
            test_caltypesbd
            ------------------------
        '''
        
        gencal(datacopy, caltable=testcal, caltype='amp', parameter=[1])
        gencal(datacopy, caltable=testcal2, caltype='sbd', parameter=[1])
        
        res1 = getParam(testcal)
        tb.open(testcal2)
        res2 = tb.getcol('FPARAM')
        tb.close()
        
        
        self.assertFalse(type(res1[0,0,0]) == type(res2[0,0,0]))
        
        
    def test_caltypembd(self):
        '''
            test_caltypembd
            ------------------------
        '''
        
        gencal(refcopy, caltable=testcal, caltype='amp', parameter=[1])
        gencal(refcopy, caltable=testcal2, caltype='mbd', parameter=[1])
        
        res1 = getParam(testcal)
        print(res1)
        tb.open(testcal2)
        res2 = tb.getcol('FPARAM')
        tb.close()
        
        
        self.assertFalse(type(res1[0,0,0]) == type(res2[0,0,0]))
        
        
    def test_caltypeopac(self):
        '''
            test_caltypeopac
            ------------------------
        '''
        
        gencal(datacopy, caltable=testcal, caltype='amp', parameter=[1])
        gencal(datacopy, caltable=testcal2, caltype='opac', parameter=[1])
        
        res1 = getParam(testcal)
        tb.open(testcal2)
        res2 = tb.getcol('FPARAM')
        tb.close()
        
        
        self.assertFalse(type(res1[0,0,0]) == type(res2[0,0,0]))
        
        
    def test_caltypeantpos(self):
        '''
            test_caltypeantpos
            ------------------------
        '''
        
        gencal(refcopy, caltable=testcal, caltype='amp', parameter=[1])
        gencal(refcopy, caltable=testcal2, caltype='antpos', parameter=[1,1,1])
        
        res1 = getParam(testcal)
        tb.open(testcal2)
        res2 = tb.getcol('FPARAM')
        tb.close()
        
        
        self.assertFalse(np.all(res1 == res2))
        
        
    def test_caltypeantposwithants(self):
        '''
            test_caltypeantposwithants
            ------------------------
        '''
        
        gencal(refcopy, caltable=testcal, caltype='amp', parameter=[1])
        gencal(refcopy, caltable=testcal2, caltype='antpos', antenna='0~5&', parameter=[1,1,1])
        
        res1 = getParam(testcal)
        tb.open(testcal2)
        res2 = tb.getcol('FPARAM')
        tb.close()
        
        
        self.assertFalse(np.all(res1 == res2))
        
        
    def test_caltypeantposvla(self):
        '''
            test_caltypeantposvla
            ------------------------
        '''
        
        gencal(refcopy, caltable=testcal, caltype='amp', parameter=[1])
        gencal(refcopy, caltable=testcal2, caltype='antposvla', antenna='0~5&', parameter=[1,1,1])
        
        res1 = getParam(testcal)
        tb.open(testcal2)
        res2 = tb.getcol('FPARAM')
        tb.close()
        
        
        self.assertFalse(np.all(res1 == res2))
        
        
    def test_caltypetsys(self):
        '''
            test_caltypetsys
            ------------------------
        '''
        
        gencal(tsyscopy, caltable=testcal2, caltype='tsys', parameter=[1])
        ans = getParamMean(testcal2)
        
        self.assertAlmostEqual(175.14380676755491, ans, places=7)
        
    
    def test_caltypetsysnonuniform(self):
        '''
            test_caltypetsysnonuniform
            ------------------------
        '''
        
        gencal(tsyscopy, caltable=testcal2, caltype='tsys', uniform=False, parameter=[1])
        ans = getParamMean(testcal2)
        
        self.assertAlmostEqual(175.14380676755491, ans, places=7)
        
        
    def test_caltypeswpow(self):
        '''
            test_caltypeswpow
            ------------------------
        '''
        
        gencal(vlacopy, caltable=testcal, caltype='amp', parameter=[1])
        gencal(vlacopy, caltable=testcal2, caltype='swpow', parameter=[1])
        
        res1 = getParam(testcal)
        tb.open(testcal2)
        res2 = tb.getcol('FPARAM')
        tb.close()
        
        self.assertFalse(np.all(res1 == res2))
        
        
    def test_caltyperq(self):
        '''
            test_caltyperq
            ------------------------
        '''
        
        gencal(vlacopy, caltable=testcal, caltype='amp', parameter=[1])
        gencal(vlacopy, caltable=testcal2, caltype='rq', parameter=[1])
        
        res1 = getParam(testcal)
        tb.open(testcal2)
        res2 = tb.getcol('FPARAM')
        tb.close()
        
        self.assertFalse(np.all(res1 == res2))
        
        
    def test_caltypeswprq(self):
        '''
            test_caltypeswprq
            ------------------------
        '''
        
        gencal(vlacopy, caltable=testcal, caltype='amp', parameter=[1])
        gencal(vlacopy, caltable=testcal2, caltype='swp/rq', parameter=[1])
        
        res1 = getParam(testcal)
        tb.open(testcal2)
        res2 = tb.getcol('FPARAM')
        tb.close()
        
        self.assertFalse(np.all(res1 == res2))
        
        
    def test_caltypegc(self):
        '''
            test_caltypegc
            ------------------------
        '''
        
        gencal(vlacopy, caltable=testcal, caltype='amp', parameter=[1])
        gencal(vlacopy, caltable=testcal2, caltype='gc', parameter=[1])
        
        res1 = getParam(testcal)
        tb.open(testcal2)
        res2 = tb.getcol('FPARAM')
        tb.close()
        
        self.assertFalse(np.all(res1 == res2))
        
    
    def test_caltypeeff(self):
        '''
            test_caltypeeff
            ------------------------
        '''
        
        gencal(vlacopy, caltable=testcal, caltype='amp', parameter=[1])
        gencal(vlacopy, caltable=testcal2, caltype='eff', parameter=[1])
        
        res1 = getParam(testcal)
        tb.open(testcal2)
        res2 = tb.getcol('FPARAM')
        tb.close()
        
        self.assertFalse(np.all(res1 == res2))
        
        
    def test_caltypegceff(self):
        '''
            test_caltypegceff
            ------------------------
        '''
        
        gencal(vlacopy, caltable=testcal, caltype='amp', parameter=[1])
        gencal(vlacopy, caltable=testcal2, caltype='gceff', parameter=[1])
        
        res1 = getParam(testcal)
        tb.open(testcal2)
        res2 = tb.getcol('FPARAM')
        tb.close()
        
        self.assertFalse(np.all(res1 == res2))
        
    
    def test_caltypetecim(self):
        '''
            test_caltypetecim
            ------------------------
        '''
        
        gencal(ngccopy, caltable=testcal, caltype='amp', parameter=[1])
        gencal(ngccopy, caltable=testcal2, caltype='tecim', infile=imref, parameter=[1])
        
        res1 = getParam(testcal)
        tb.open(testcal2)
        res2 = tb.getcol('FPARAM')
        tb.close()
        
        self.assertFalse(np.all(res1 == res2))
        
        
    def test_spw(self):
        '''
            test_spw
            ------------------------
        '''
        
        gencal(datacopy, caltable=testcal, caltype='ph', spw='1', parameter=[1])
        gencal(datacopy, caltable=testcal2, caltype='ph', parameter=[1])
        ans1 = getParamMean(testcal, 'CPARAM')
        ans2 = getParamMean(testcal2, 'CPARAM')
        
        self.assertFalse(ans1 == ans2)
        
        
    def test_antenna(self):
        '''
            test_antenna
            ------------------------
        '''
        
        gencal(refcopy, caltype='antpos', caltable=testcal, antenna='0', parameter=[1,1,1])
        gencal(refcopy, caltype='antpos', caltable=testcal2, antenna='0~5&', parameter=[1,1,1])
        
        ans1 = getParamMean(testcal)
        ans2 = getParamMean(testcal2)
        
        self.assertFalse(ans1 == ans2)
        
        
    def test_pol(self):
        '''
            test_pol
            ------------------------
        '''
        
        gencal(refcopy, caltype='ph', caltable=testcal, pol='R', parameter=[1])
        gencal(refcopy, caltype='ph', caltable=testcal2, pol='R,L', parameter=[1])
        
        ans1 = getParamMean(testcal, 'CPARAM')
        ans2 = getParamMean(testcal2, 'CPARAM')
        
        self.assertFalse(ans1 == ans2)
        
        
    def test_parameter(self):
        '''
            test_parameter
            ------------------------
        '''
        
        gencal(refcopy, caltype='ph', caltable=testcal, pol='R', parameter=[1])
        gencal(refcopy, caltype='ph', caltable=testcal2, pol='R,L', parameter=[10])
        
        ans1 = getParamMean(testcal, 'CPARAM')
        ans2 = getParamMean(testcal2, 'CPARAM')
        
        self.assertFalse(ans1 == ans2)
        
        
    def test_uniform(self):
        '''
            test_uniform
            ------------------------
        '''
        
        gencal(tsyscopy, caltable=testcal, caltype='tsys', uniform=True, parameter=[1])
        gencal(tsyscopy, caltable=testcal2, caltype='tsys', uniform=False, parameter=[1])
        
        ans1 = getParam(testcal, 'SNR')
        ans2 = getParam(testcal2, 'SNR')
        
        self.assertFalse(np.all(ans1 == ans2))
        
    # Check specialized caltypes aren't effected by selection parameters
        
def suite():
    return[gencal_test]

if __name__ == "__main__":
    unittest.main()
