########################################################################
# test_req_task_split.py
#
# Copyright (C) 2018
# Associated Universities, Inc. Washington DC, USA
#
# This script is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# [Add the link to the JIRA ticket here once it exists]
#
# Based on the requirements listed in plone found here:
# https://casa.nrao.edu/casadocs-devel/stable/global-task-list/task_split/about
#
#
##########################################################################

CASA6 = False

try:
    import casatools
    from casatasks import split, casalog, rmtables, flagdata
    CASA6 = True
except ImportError:
    from __main__ import default
    from tasks import *
    from taskinit import *
    import casaTestHelper
import sys
import os
import unittest
import shutil
import numpy
import casaTestHelper

ms = mstool()
msmd = msmdtool()

if CASA6:
    mspath = casatools.ctsys.resolve('visibilities/alma/Itziar.ms')
    mmspath = casatools.ctsys.resolve('visibilities/other/outlier_mms.mms')
    intentpath = casatools.ctsys.resolve('visibilities/alma/nep2-shrunk.ms')
    spwpath = casatools.ctsys.resolve('visibilities/alma/uid___A002_X30a93d_X43e_small.ms')
    floatpath = casatools.ctsys.resolve('visibilities/other/analytic_spectra_tsys.ms')
else:
    if os.path.exists(os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req'):
        dataroot = os.environ.get('CASAPATH').split()[0] + '/'
        mspath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/alma/Itziar.ms'
        mmspath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/other/outlier_mms.mms'
        intentpath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/alma/nep2-shrunk.ms'
        spwpath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/alma/uid___A002_X30a93d_X43e_small.ms'
        floatpath = os.environ.get('CASAPATH').split()[0] + '/data/casa-data-req/visibilities/other/analytic_spectra_tsys.ms'
    else:
        dataroot = os.environ.get('CASAPATH').split()[0] + '/'
        mspath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/alma/Itziar.ms'
        mmspath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/other/outlier_mms.mms'
        intentpath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/alma/nep2-shrunk.ms'
        spwpath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/alma/uid___A002_X30a93d_X43e_small.ms'
        floatpath = os.environ.get('CASAPATH').split()[0] + '/casa-data-req/visibilities/other/analytic_spectra_tsys.ms'
        
logpath = casalog.logfile()

class split_test(unittest.TestCase):
    
    def setUp(self):

        if not CASA6:
            default(split)
        else:
            pass
            
    def tearDown(self):
        casalog.setlogfile(logpath)
        if os.path.exists('testlog.log'):
            os.remove('testlog.log')
            
    def test_takesMeasurementSet(self):
        ''' 1. test_takesMeasurementSet: tests if split accepts an MS'''
        success = split(vis=mspath, outputvis='test.ms', spw='0', datacolumn='all')
        shutil.rmtree('test.ms')
        self.assertTrue(success)
        
    def test_takesMultiMeasurementSet(self):
        '''2. test_takesMultiMeasurementSet: tests if split accepts an MMS '''
        success = split(vis=mmspath, outputvis='test.mms', spw='0', datacolumn='all')
        shutil.rmtree('test.mms')
        self.assertTrue(success)
        
    def test_producesOutputvis(self):
        ''' 3. test_producesOutputvis: tests if split produces a file with the correct file name '''
        split(vis=mspath, outputvis='outputtest.ms', spw='0', datacolumn='all')
        success = os.path.exists('outputtest.ms')
        shutil.rmtree('outputtest.ms')
        self.assertTrue(success)
         
    def test_keepMMS(self):
        ''' 4. test_keepMMS: tests if an MMS file is produced if keepmms = True '''
        split(vis=mmspath, outputvis='keepmmstest.ms', keepmms=True, datacolumn='all')
        ms.open(mmspath)
        ismms = ms.ismultims()
        ms.done()
        shutil.rmtree('keepmmstest.ms')
        self.assertTrue(ismms)
        
    def test_datacolumnOptionCorrected(self):
        ''' 5. test_datacolumnOptionCorrected: tests if split works with data column = 'corrected' '''
        success = split(vis=mspath, outputvis='correctedtest.ms', datacolumn='corrected')
        shutil.rmtree('correctedtest.ms')
        self.assertTrue(success)
        
    def test_datacolumnOptionData(self):
        ''' 6. test_datacolumnOptionData: tests if split works with data column = 'data' '''
        success = split(vis=mspath, outputvis='datatest.ms', datacolumn='data')
        shutil.rmtree('datatest.ms')
        self.assertTrue(success)
        
    def test_datacolumnOptionModel(self):
        ''' 7. test_datacolumnOptionModel: tests if split works with data column = 'model' '''
        success = split(vis=mspath, outputvis='modeltest.ms', datacolumn='model')
        shutil.rmtree('modeltest.ms')
        self.assertTrue(success)
        
    def test_datacolumnOptionDMC(self):
        ''' 8. test_dataOptionDMC: tests if split works with data column = 'data, model, corrected' '''
        success = split(vis=mspath, outputvis='cdmtest.ms', datacolumn='data,model,corrected')
        shutil.rmtree('cdmtest.ms')
        self.assertTrue(success)
        
    def test_datacolumnOptionFloat(self):
        ''' 9. test_datacolumnOptionFloat: tests if split works with data column = 'float_data' '''
        success = split(vis=floatpath, outputvis='floattest.ms', datacolumn='float_data')
        shutil.rmtree('floattest.ms')
        self.assertTrue(success)
        
   # def test_datacolumnOptionFloatData(self):
    #    ''' 10. test_datacolumnOptionFloatData: test if split works with data column = 'float_data,data' '''
    #    tb.open(floatpath)
    #    tb.copy('floatdatatest.ms')
        
        
        #success = split(vis=mspath, outputvis='lagtest.ms', datacolumn='lag_data')
        #self.assertTrue(success)

        #success = split(vis=mspath, outputvis='floatdatatest.ms', datacolumn='float_data,data')
        #self.assertTrue(success)
        #success = split(vis=mspath, outputvis='lagdatatest.ms', datacolumn='lag_data,data')
        #self.assertTrue(success)
    def test_datacolumnAll(self):
        ''' 10. test_datacolumnAll: tests if split works with datacolumn = 'all' '''
        success = split(vis=mspath, outputvis='alltest.ms', datacolumn='all')
        shutil.rmtree('alltest.ms')
        self.assertTrue(success)

    def test_defaultcorrecteddata(self):
        ''' 11. test_defaultcorrecteddata: tests if split returns the correct column for the default value of datacolumn '''
        split(vis=mspath, outputvis='defaultdatatest.ms')
        tb.open('defaultdatatest.ms', nomodify=False)
        tb.renamecol('DATA', 'CORRECTED_DATA')
        tb.close()
        casaTestHelper.compare_CASA_variable_cols(referencetab=mspath, testtab='defaultdatatest.ms', varcol='CORRECTED_DATA')
            
    def test_keepflagsTrue(self):
        ''' 12. test_keepflagsTrue: tests if completely flagged data is not dropped if keepflags = True '''
        tb.open(mspath)
        tb.copy(newtablename='flagdatatruetest.ms')
        flagdata('flagdatatruetest.ms', field='0~5')
        split(vis='flagdatatruetest.ms', outputvis='splitflagdatatrue.ms', datacolumn='data')
        casaTestHelper.compare_CASA_variable_cols(referencetab='flagdatatruetest.ms', testtab='splitflagdatatrue.ms', varcol='DATA')
        rmtables(tablenames = ['flagdatatruetest.ms', 'splitflagdatatruetest.ms'])
        
    def test_keepflagsFalse(self):
        ''' 13. test_keepflagsFalse: tests if completely flagged data is dropped from table produced by split '''
        tb.open(mspath)
        tb.copy(newtablename='flagdatafalsetest.ms')
        flagdata('flagdatafalsetest.ms', field='0~5')
        split(vis='flagdatafalsetest.ms', outputvis='splitflagdatafalse.ms', datacolumn='data')
        not(casaTestHelper.compare_CASA_variable_cols(referencetab='flagdatafalsetest.ms', testtab='splitflagdatafalse.ms', varcol='DATA'))
        rmtables(tablenames=['flagdatafalsetest.ms', 'splitflagdatafalse.ms'])
        
    def test_width(self):
        ''' 14. test_width: tests if the value specified in width sets the number of input channels to average '''
        # split(vis=mspath, outputvis='widthtest.ms', width=2, datacolumn='data')
        # ms.open('widthtest.ms')
        # rec = ms.getdata("data")
        # result = rec.get("data")[0][0][0:10]
        # originalresult = numpy.array([ 0.00352126 +4.08124070e-05j,  0.00032893 +2.24740920e-03j, 
        # 0.00042682 +5.59682521e-05j,  0.00034747 -1.15316745e-03j,-0.00108992 +4.25997860e-04j,  
        # 0.00052187 +1.46636317e-04j,0.00042417 +8.21641064e-04j, -0.00032741 +5.33973682e-04j,
        # -0.00098293 +2.02878364e-04j, -0.00012852 +1.77519134e-04j])
        # 
        # self.assertTrue(result.all() == originalresult.all())

        split(vis=intentpath, outputvis='widthtest.ms', width=2, datacolumn='all')
        ms.open(intentpath)
        rec=ms.getdata('data')
        datarow=rec.get('data')
        example1 = datarow[0][0][0]
        example2 = datarow[0][1][0]
        averageOrigData = (example1 + example2)/2
        ms.close()
        
        ms.open('widthtest.ms')
        rec=ms.getdata('data')
        datarow = rec.get('data')
        ms.close()
        
        self.assertTrue(averageOrigData == datarow[0][0][0])
        
    def test_defaultWidth(self):
        ''' 15. test_defaultWidth: tests if the default value of width of 1 is used (no averaging) '''
        ms.open(intentpath)
        rec = ms.getdata('data')
        datarow = rec.get('data')
        example1 = datarow[0][0][0]
        ms.done()
        
        split(vis=intentpath, outputvis='defaultwidth.ms', datacolumn='data')
        ms.open('defaultwidth.ms')
        rec = ms.getdata('data')
        datarow = rec.get('data')
        example2 = datarow[0][0][0]
        ms.done()
        shutil.rmtree('defaultwidth.ms')
        
        self.assertTrue(example1.real == example2.real and example1.imag == example2.imag)
        
    def test_selectField(self):
        ''' 16. test_selectField: tests if the field selection parameter works '''
        split(vis=mspath, outputvis='fieldselectiontest.ms', field='Itziar_0')
        msmd.open('fieldselectiontest.ms')
        fieldnames = msmd.fieldnames()
        msmd.done()
        shutil.rmtree('fieldselectiontest.ms')
        self.assertTrue(len(fieldnames) == 1 and fieldnames[0] == 'Itziar_0')

    def test_defaultField(self):
        ''' 17. test_defaultField: tests if the default value of field returns all fields '''
        msmd.open(mspath)
        fieldnames = msmd.fieldnames();
        msmd.done()
        split(vis=mspath, outputvis='fielddefaulttest.ms', spw='0')
        msmd.open('fielddefaulttest.ms')
        splitfieldnames = msmd.fieldnames()
        msmd.done()
        shutil.rmtree('fielddefaulttest.ms')
        self.assertTrue(splitfieldnames == fieldnames)
        
    def test_selectSpw(self):
        ''' 18. test_selectSpw: tests if the spectral window selection parameter works '''
        split(vis=spwpath, outputvis='spwselectiontest.ms', spw='0~1', datacolumn='data')
        ms.open('spwselectiontest.ms')
        splitSpwInfo = ms.getspectralwindowinfo()
        splitSpwFreq0 = splitSpwInfo['0']['Chan1Freq']
        splitSpwFreq1 = splitSpwInfo['1']['Chan1Freq']
        ms.done()
        shutil.rmtree('spwselectiontest.ms')
        
        ms.open(spwpath)
        spwInfo = ms.getspectralwindowinfo()
        spwFreq0 = spwInfo['0']['Chan1Freq']
        spwFreq1 = spwInfo['1']['Chan1Freq']
        ms.done()
        
        self.assertTrue(splitSpwFreq0 == spwFreq0 and splitSpwFreq1 == spwFreq1)
        
    def test_defaultSpw(self):
        ''' 19. test_defaultSpw: tests is default value of spw returns all spectral windows '''
        split(vis=spwpath, outputvis='defaultspwtest.ms', datacolumn='data')
        ms.open('defaultspwtest.ms')
        splitSpwInfo = ms.getspectralwindowinfo()
        freq1 = []
        freq1.append(splitSpwInfo['0']['Chan1Freq'])
        freq1.append(splitSpwInfo['1']['Chan1Freq'])
        freq1.append(splitSpwInfo['2']['Chan1Freq'])
        freq1.append(splitSpwInfo['3']['Chan1Freq'])
        ms.done()
        
        ms.open(spwpath)
        spwInfo = ms.getspectralwindowinfo()
        freq2 = []
        freq2.append(spwInfo['0']['Chan1Freq'])
        freq2.append(spwInfo['1']['Chan1Freq'])
        freq2.append(spwInfo['2']['Chan1Freq'])
        freq2.append(spwInfo['3']['Chan1Freq'])
        ms.done()
        
        shutil.rmtree('defaultspwtest.ms')
        
        self.assertTrue(freq1 == freq2)
                
    def test_selectAntenna(self):
        ''' 20. test_selectAntenna: tests if the antenna selection parameter works '''
        split(vis=mspath, outputvis='antennaselecttest.ms', antenna='A00&A01')
        msmd.open('antennaselecttest.ms')
        antennas = msmd.antennanames()
        msmd.done()
        shutil.rmtree('antennaselecttest.ms')
        self.assertTrue(antennas == ['A00', 'A01'])
        
    def test_defaultAntenna(self):
        ''' 21. test_defaultAntenna: tests if the default antenna parameter returns all antennas '''
        msmd.open(mspath)
        antennas = msmd.antennanames()
        msmd.done()
        split(vis=mspath, outputvis='antennadefaulttest.ms')
        msmd.open('antennadefaulttest.ms')
        splitantennas = msmd.antennanames()
        msmd.done()
        shutil.rmtree('antennadefaulttest.ms')
        self.assertTrue(splitantennas == antennas)
        
    def test_selectCorrelation(self):
        ''' 22. test_selectCorrelation: tests if the correlation selection parameter works '''
        split(vis=mspath, outputvis='selectarraytest.ms', correlation='XX')
        msmd.open('selectarraytest.ms')
        polid = msmd.polidfordatadesc(-1)
        corrtypes = msmd.corrtypesforpol(polid[0])
        msmd.done()
        shutil.rmtree('selectarraytest.ms')
        self.assertTrue(len(corrtypes)==1 and corrtypes[0] == 9)
        
    def test_defaultCorrelation(self):
        ''' 23. test_defaultCorrelation: tests if the default correlation parameter returns all correlations '''
        split(vis=mspath, outputvis='correlationdefault.ms')
        msmd.open('correlationdefault.ms')
        polid = msmd.polidfordatadesc(-1)
        corrtypes = msmd.corrtypesforpol(polid[0])
        msmd.done()
        shutil.rmtree('correlationdefault.ms')
        
        msmd.open(mspath)
        origpolid = msmd.polidfordatadesc(-1)
        origcorrtypes = msmd.corrtypesforpol(origpolid[0])
        msmd.done()
        
        self.assertTrue(corrtypes.all() == origcorrtypes.all())
        
    def test_selectTimerange(self):
        ''' 24. test_selectTimerange: tests if data can be selected based on time range '''
        split(vis=mspath, outputvis='timerange.ms', timerange='10:16:45~10:17:15')
        ms.open('timerange.ms', nomodify=False)
        d = ms.getdata(['axis_info'], ifraxis=True)
        tstart = min(d['axis_info']['time_axis']['MJDseconds'])/86400
        tstop = max(d['axis_info']['time_axis']['MJDseconds'])/86400
        ms.close()
        shutil.rmtree('timerange.ms')
        self.assertTrue=(tstart >= 56099.428598314036 and tend <= 56099.42864583)
        
    def test_defaultTimerange(self):
        ''' 25. test_defaultTimerange: tests if default value of timerange returns the entire time range '''
        ms.open(mspath)
        d = ms.getdata(['axis_info'], ifraxis=True)
        tstart = min(d['axis_info']['time_axis']['MJDseconds'])
        tstop = max(d['axis_info']['time_axis']['MJDseconds'])
        ms.close()
        
        split(vis=mspath, outputvis='defaulttimerange.ms')
        ms.open('defaulttimerange.ms')
        d = ms.getdata(['axis_info'], ifraxis=True)
        splittstart = min(d['axis_info']['time_axis']['MJDseconds'])
        splitstop = max(d['axis_info']['time_axis']['MJDseconds'])
        ms.close()
        shutil.rmtree('defaulttimerange.ms')
        self.assertTrue(splittstart == tstart and splitstop == tstop)
        
    def test_selectIntent(self):
        ''' 26. test_selectIntent : tests if the intent selection parameter works '''
        split(vis=intentpath, outputvis='intentselectiontest.ms', intent = 'CALIBRATE_BANDPASS#ON_SOURCE', datacolumn = 'all')
        msmd.open('intentselectiontest.ms')
        intent = msmd.intents()
        msmd.done()
        shutil.rmtree('intentselectiontest.ms')
        self.assertTrue(intent[0] == 'CALIBRATE_BANDPASS#ON_SOURCE')
        
    def test_defaultIntent(self):
        ''' 27. test_defaultIntent: tests if the default value of intent returns all intents '''
        msmd.open(intentpath)
        intents = msmd.intents()
        msmd.done()
        split(vis=intentpath, outputvis='intentdefaulttest.ms', spw='0', datacolumn = 'all')
        msmd.open('intentdefaulttest.ms')
        splitintents = msmd.intents()
        msmd.done()
        shutil.rmtree('intentdefaulttest.ms')
        self.assertTrue(splitintents == intents)
        
    def test_selectArray(self):
        ''' 28. test_selectArray: tests if the array selection parameter works. '''
        tb.open(mspath)
        tb.copy(newtablename='multiplearray.ms')
        tb.close()
        
        tb.open('multiplearray.ms', nomodify=False)
        arrays = tb.getcol('ARRAY_ID')
        arrays[0:120000] = 1
        tb.putcol('ARRAY_ID', arrays)
        tb.flush()
        tb.close()
        
        split(vis='multiplearray.ms', outputvis='selectarraytest.ms', array = 0)
        tb.open('selectarraytest.ms')
        arrays = tb.getcol('ARRAY_ID')
        tb.close()
        
        shutil.rmtree('multiplearray.ms')
        shutil.rmtree('selectarraytest.ms')
        
        uniquearrays = numpy.unique(arrays)
        self.assertTrue(uniquearrays == 0)
        
    def test_defaultArray(self):
        ''' 29. test_defaultArray: tests if the default value of array returns all arrays '''
        tb.open(mspath)
        tb.copy(newtablename='multiplearray2.ms')
        tb.close()
        
        tb.open('multiplearray2.ms', nomodify=False)
        arrays = tb.getcol("ARRAY_ID")
        arrays[0:120000] = 1
        tb.putcol('ARRAY_ID', arrays)
        tb.flush()
        tb.close()
        
        tb.open('multiplearray2.ms')
        arrays = tb.getcol('ARRAY_ID')
        tb.close()
        uniquearrays = numpy.unique(arrays)
        
        split(vis='multiplearray2.ms', outputvis='defaultarraytest.ms')
        tb.open('defaultarraytest.ms')
        arrays = tb.getcol('ARRAY_ID')
        tb.close()
        shutil.rmtree('multiplearray2.ms')
        shutil.rmtree('defaultarraytest.ms')
        uniquesplitarrays = numpy.unique(arrays)
        self.assertTrue(uniquesplitarrays.all() == uniquearrays.all())
        
    def test_selectUvrangeMeters(self):
        ''' 30. test_selectUvrange: tests if data can be selected by baseline length in meters '''
        ms.open(mspath)
        uvrange = ms.range('uvdist')
        ms.close()
        uvrangemin = uvrange['uvdist'][0]
        uvrangemax = uvrange['uvdist'][1]
        
        split(vis=mspath, outputvis='metersrangetest.ms', uvrange='20~50')
        ms.open('metersrangetest.ms')
        uvrangemeters = ms.range('uvdist')
        uvrangemetersmin = uvrangemeters['uvdist'][0]
        uvrangemetersmax = uvrangemeters['uvdist'][1]
        ms.close()
        shutil.rmtree('metersrangetest.ms')
        
        self.assertTrue(uvrangemetersmin >= uvrangemin and uvrangemetersmax <= uvrangemax)
        
    def test_selectUvrangeKilometers(self):
        ''' 31. test_selectUvrangeKilometers: tests if data can be selected by baseline in kilometers '''
        ms.open(mspath)
        uvrange = ms.range('uvdist')
        ms.close()
        uvrangemin = uvrange['uvdist'][0]
        uvrangemax = uvrange['uvdist'][1]
        
        split(vis=mspath, outputvis='kmrangetest.ms', uvrange='0.02~0.05km')
        ms.open('kmrangetest.ms')
        uvrangekmmin = uvrange['uvdist'][0]
        uvrangekmmax = uvrange['uvdist'][1]
        ms.close()
        shutil.rmtree('kmrangetest.ms')
        
        self.assertTrue(uvrangekmmin >= uvrangemin and uvrangekmmax <= uvrangemax)
        
    def test_selectUvrangeLambdas(self):
        ''' 32. test_selectUvrangeLambdas: tests if data can be selected by baseline in lambdas '''
        ms.open(mspath)
        uvrange = ms.range('uvdist')
        uvrangemin = uvrange['uvdist'][0]
        uvrangemax = uvrange['uvdist'][1]
        
        c =  299792458
        freq = ms.range('ref_frequency')['ref_frequency'][0]
        ms.close()
        lambdamin = 20 * freq / c
        lambdamax = 50 * freq /c
        uvrangestring = str(lambdamin) + '~' + str(lambdamax) + 'lambda'
        print(uvrangestring)
        split(vis=mspath, outputvis='lambdarangetest.ms', uvrange=uvrangestring)
        ms.open('lambdarangetest.ms')
        uvrangelambda = ms.range('uvdist')
        uvrangelambdamin = uvrangelambda['uvdist'][0]
        uvrangelambdamax = uvrangelambda['uvdist'][1]
        ms.close()
        shutil.rmtree('lambdarangetest.ms')
        
        self.assertTrue(uvrangelambdamin >= uvrangemin and uvrangelambdamax <= uvrangemax)
        
    def test_selectUvrangeKilolambdas(self):
        ''' 33. test_selectUvrangeKilolambdas: tests if data can be selected by baseline in kilolambdas '''
        ms.open(mspath)
        uvrange = ms.range('uvdist')
        uvrangemin = uvrange['uvdist'][0]
        uvrangemax = uvrange['uvdist'][1]
        
        c =  299792458
        freq = ms.range('ref_frequency')['ref_frequency'][0]
        ms.close()
        lambdamin = 20 * freq / c
        lambdamax = 50 * freq /c
        
        klambdamin = lambdamin / 1000;
        klambdamax = lambdamax / 1000;
        split(vis=mspath, outputvis='klambdatest.ms', uvrange=str(klambdamin) + '~' + str(klambdamax) + 'klambda')
        ms.open('klambdatest.ms')
        uvrangeklambda = ms.range('uvdist')
        uvrangeklambdamin = uvrangeklambda['uvdist'][0]
        uvrangeklambdamax = uvrangeklambda['uvdist'][1]
        ms.close()
        shutil.rmtree('klambdatest.ms')
        
        self.assertTrue(uvrangeklambdamin >= uvrangemin and uvrangeklambdamax <= uvrangemax) 
        
    def test_defaultUvrange(self):
        ''' 34. test_defaultUvrange: tests if the default value of uvrange returns all data '''
        ms.open(mspath)
        ranges = ms.range('uvdist')['uvdist']
        ms.close()
        
        split(vis=mspath, outputvis='defaultRange.ms')
        ms.open('defaultRange.ms')
        splitranges = ms.range('uvdist')['uvdist']
        ms.close()
        shutil.rmtree('defaultRange.ms')
        
        self.assertTrue(splitranges.all() == ranges.all())
        

              
def suite():
    return[split_test]
        
# Main #
if __name__ == '__main__':
    unittest.main()